;
(function () {

    'use strict';

    // Dependencies
    var express = require('express'),
        logger = require('morgan');

    // App dependencies
    var app = express(),
        PORT =  process.env.PORT || 3011;

    // Log history in to the console
    app.use(logger('dev'));

    // Allow CORS
    app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST', 'PUT', 'DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
        next();
    });

    // Set port for express server
    app.set('port', PORT);
    
    // Routes dependency and pass app to router 
    require('./api_routes/routes')(app);

    // PORT listener
    app.listen(PORT, function (error) {
        if (error) throw error;
        console.log('Express server listening on port ' + PORT);
    });

}());