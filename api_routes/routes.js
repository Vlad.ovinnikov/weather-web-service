/**
 * Created by vlad on 22/09/16.
 */
;
(function () {

    'use strict';

    module.exports = function (app) {

        // Dependencies
        var router = require('express').Router(),
            weatherApi = require('./weather_api');
        
        app.use(router);

        // Weather routes
        router.get('/weather', weatherApi.getWeather);
        router.get('/list', weatherApi.getWeatherList);

    }
}());