/**
 * Created by vlad on 22/09/16.
 */
;
(function () {
    'use strict';

    // Dependencies
    var YQL = require('yql');

    module.exports = {
        
        // GET weather for a city
        getWeather: function (req, res, next) {
            // get request params
            var queryParams = req.query,
                zip = queryParams.zip,
                query = new YQL('select item from weather.forecast where woeid in (select woeid from geo.places where text=' + zip + ') LIMIT 1');

            query.exec(function (err, data) {
                return res.status(200).json(data.query.results);
            });
        },

        //GET weather for city list
        getWeatherList: function (req, res, next) {
            // get request params
            var queryParams = req.query,
                zip = queryParams.zip,
                query = new YQL('select item from weather.forecast where woeid in (select woeid from geo.places where text in (' + zip + '))');

            query.exec(function (err, data) {
                return res.status(200).json(data.query.results);
            });
        }
    }

}());